#[cfg(test)]
mod tests {
    use crate::base64::Base64;

    fn setup() -> Base64 {
        Base64::new()
    }

    #[test]
    fn base64_encode_test() {
        let string = "When casting to a signed type, the (bitwise) result is the same as";
        let mut b64 = setup();
        let res = b64.encode(&String::from(string));
        assert_eq!(res, "V2hlbiBjYXN0aW5nIHRvIGEgc2lnbmVkIHR5cGUsIHRoZSAoYml0d2lzZSkgcmVzdWx0IGlzIHRoZSBzYW1lIGFz");
    }

    #[test]
    fn base64_decode_test() {
        let string = "V2hlbiBjYXN0aW5nIHRvIGEgc2lnbmVkIHR5cGUsIHRoZSAoYml0d2lzZSkgcmVzdWx0IGlzIHRoZSBzYW1lIGFz";
        let mut b64 = setup();
        let res = b64.decode(&String::from(string));
        assert_eq!(res, "When casting to a signed type, the (bitwise) result is the same as");
    }

    #[test]
    #[should_panic]
    fn base64_decode_test_invalid_input() {
        let string = "V2hlbiBjYXN0aW5nIHRvIGEgc2lnbmVkIHR5cGUsIHRoZSAoYml0d2l*?/||";
        let mut b64 = setup();
        b64.decode(&String::from(string));
    }
}
