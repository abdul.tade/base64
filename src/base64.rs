use core::panic;
use std::collections::HashMap;

pub struct Base64 {
    lookup: [char; 64],
    group_size: usize,
    padding: usize,
    at_end: bool,
    is_populated: bool,
    reverse_lookup: HashMap<char, usize>,
}

impl Default for Base64 {
    fn default() -> Self {
        Base64 {
            lookup: [
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
            ],
            group_size: 3,
            padding: 0,
            at_end: false,
            is_populated: false,
            reverse_lookup: HashMap::new(),
        }
    }
}

impl Base64 {
    pub fn encode(&mut self, string: &String) -> String {
        let mut encoded_string = String::new();
        let mut index = 0usize;
        let mut byte_vector = Vec::from(string.as_bytes());
        self.pad(&mut byte_vector);

        loop {
            if index > byte_vector.len() - 1 {
                break;
            }
            let group_arr = [
                byte_vector[index],
                byte_vector[index + 1],
                byte_vector[index + 2],
            ];

            index += 3;
            self.at_end = index > (byte_vector.len() - 1);

            let encoded_group = self.encode_group(&group_arr);
            encoded_string += encoded_group.as_str();
        }

        encoded_string
    }

    pub fn new() -> Base64 {
        Base64{..Default::default()}
    }

    fn pad(&mut self, bytes: &mut Vec<u8>) {
        let byte_len = bytes.len();
        if byte_len % self.group_size != 0usize {
            let bytes_to_add = self.group_size - (byte_len % self.group_size);
            self.padding = bytes_to_add;
            for _ in 0..bytes_to_add {
                bytes.push(0);
            }
        }
    }

    fn encode_group(&self, group_arr: &[u8; 3]) -> String {
        let mut ret_vec: Vec<char> = Vec::new();
        let byte = group_arr[0];

        let prev: usize = (byte as usize) & 3;
        ret_vec.push(self.lookup[(byte as usize) >> 2]);

        let byte = group_arr[1];
        let byte_index = ((byte as usize) >> 4) | (prev << 4);
        let prev = ((byte & 15) << 2) as usize;
        ret_vec.push(self.lookup[byte_index]);

        let byte = group_arr[2];
        let byte_index = ((byte as usize) >> 6) | prev;
        let prev = (byte & 63) as usize;
        ret_vec.push(self.lookup[byte_index]);

        ret_vec.push(self.lookup[prev]);

        if (self.padding != 0) && self.at_end {
            let mut index = self.padding;
            while index > 0 {
                ret_vec[4 - index] = '=';
                index -= 1;
            }
        }

        let mut ret_string = String::new();
        ret_string.extend(ret_vec.iter());
        ret_string
    }

    fn append_and_split_chars(&self, group_arr: [u8; 4]) -> [char; 3] {
        let mut res_group: [char; 3] = ['0', '0', '0'];
        let mut value = 0u32;

        value = value | ((group_arr[0] as u32) << 18);
        value = value | ((group_arr[1] as u32) << 12);
        value = value | ((group_arr[2] as u32) << 6);
        value = value | (group_arr[3] as u32);

        res_group[2] = ((value & 255) as u8) as char;
        res_group[1] = (((value >> 8) & 255) as u8) as char;
        res_group[0] = (((value >> 16) & 255) as u8) as char;

        res_group
    }

    fn decode_error(&self, condition: bool) {
        if !condition {
            panic!("Invalid base64 character");
        }
    }

    fn decode_group(&self, group_arr: &mut [char; 4]) -> Vec<char> {
        let mut byte_vector: Vec<char> = Vec::new();

        println!("{:?}", group_arr);

        let b0 = self.reverse_lookup.get(&group_arr[0]);
        self.decode_error(b0 != None);

        let b1 = self.reverse_lookup.get(&group_arr[1]);
        self.decode_error(b1 != None);

        let b2 = self.reverse_lookup.get(&group_arr[2]);
        self.decode_error(b2 != None);

        let b3 = self.reverse_lookup.get(&group_arr[3]);
        self.decode_error(b3 != None);

        let appended_bytes = self.append_and_split_chars([
            *b0.unwrap() as u8,
            *b1.unwrap() as u8,
            *b2.unwrap() as u8,
            *b3.unwrap() as u8,
        ]);

        byte_vector.extend(appended_bytes.iter());
        byte_vector
    }

    pub fn decode(&mut self, string: &String) -> String {
        if string.len() % 4 != 0 {
            panic!("Invalid base64 encoding");
        }

        if !self.is_populated {
            for i in 0..self.lookup.len() {
                self.reverse_lookup.insert(self.lookup[i], i);
            }
            self.reverse_lookup.insert('=', 64);
            self.is_populated = true;
        }

        self.padding = string.matches("=").count();
        let char_vector: Vec<char> = string.chars().collect();
        let mut decoded_vector: Vec<char> = Vec::new();
        let mut decoded_string = String::new();
        let mut index = 0usize;

        loop {
            if index > char_vector.len() - 1 {
                break;
            }

            let mut group_arr = [
                char_vector[index],
                char_vector[index + 1],
                char_vector[index + 2],
                char_vector[index + 3],
            ];

            index += 4;
            self.at_end = index > char_vector.len() - 1;
            let mut decoded_group = self.decode_group(&mut group_arr);
            decoded_vector.append(&mut decoded_group);
        }

        decoded_string.extend(decoded_vector.iter());
        decoded_string
    }
}
