
mod base64;
mod test;
use base64::Base64;

fn main() {
    let mut b64 = Base64::new();
    let string =  String::from("When casting to a signed type, the (bitwise) result is the same as");
    let res = b64.encode(&string);
    let res_decode = b64.decode(&res);
    println!("result = {}, {}", res, res_decode);
}


