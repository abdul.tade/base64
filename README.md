# Base64 Encoding and Decoding

Rust has its own base64 library. But I wanted to practice so I decided to
write one my self. Why you might ask ? Well why not. 😂 😂 😂

## Example

```rust
use base64::Base64;

fn main() {
    let mut b64 = Base64::new();
    let string =  String::from("When casting to a signed type, the (bitwise) res");
    let res = b64.encode(&string);
    let res_decode = b64.decode(&res);
    println!("result = {}, {}", res, res_decode);
}
```

# Links

* **Link to read about base64 encoding** [https://base64.guru/learn/base64-algorithm/decode]